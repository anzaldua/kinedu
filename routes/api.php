<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1'], function(){

	Route::get('index', 'ProductController@index')->name('showProducts');

	Route::get('show/{id}', 'ProductController@show')->name('showProduct');

	Route::get('create', 'ProductController@create')->name('createProduct');

	Route::put('update/{id}', 'ProductController@update')->name('updateProduct');

	Route::delete('delete/{id}', 'ProductController@destroy')->name('deleteProduct');

	Route::post('restore/{id}', 'ProductController@restore')->name('restoreProduct');

});
