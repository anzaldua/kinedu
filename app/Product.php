<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'Nombre', 'Descripcion', 'Precio', 'UnidadDeMedida', 'Marca', "deleted_at",
    ];

    protected $dates = ['delete_at'];
}
