<?php

namespace App\Listeners;

use App\Events\EventoProducto;
use App\Mail\CorreoProducto;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Events\Event;

class ProductoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CambioProducto  $event
     * @return void
     */
    public function handle(EventoProducto $event)
    {
        Mail::to($event->datos['adminEmail'])->send( new CorreoProducto( $event->datos ) );
    }
}
