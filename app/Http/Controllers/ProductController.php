<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Events\EventoProducto;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        /* Se agrega autenticacion a todos los metodos menos el eventoEmail*/
        
        $user = $this->middleware('auth:api', ['only'=>['index']]);
        $user = $this->middleware('auth:api', ['only'=>['show']]);
        $user = $this->middleware('auth:api', ['only'=>['create']]);
        $user = $this->middleware('auth:api', ['only'=>['update']]);
        $user = $this->middleware('auth:api', ['only'=>['destroy']]);
        $user = $this->middleware('auth:api', ['only'=>['restore']]);

    }

    public function eventoEmail(){

        /*===========================================================================
        = Variable de email Super Admin y se envia a evento como array  =
        =======================================================================*/

        $adminEmail = User::where('role_id', 1)->select('email')->get();

        $cambio = array('adminEmail'=>$adminEmail); 

        $evento = event(new EventoProducto($cambio));

        return $evento;
    }
    /**
     * Permite ver todos los productos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();

        $role = Auth::user();

        if($role->Administrador()){

            $respuesta = Response::json($product, 200);

            return $respuesta;

        }else{

            return "Acceso no autorizado";

        }

    }

    /**
     * Permite ver un producto en base a su ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if(!$product){
            return Response::json([

                'error' => [

                    'message' => "No se ha encontrado producto con ese ID."

                ]

            ], 404);
        }
    
        $role = Auth::user();

        if($role->Administrador()){

            return Response::json($product, 200);

        }else{

            return "Acceso no autorizado";

        }
    }

    /**
     * Permite crear un nuevo producto
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $product = new Product();

        $role = Auth::user();

        if($role->Administrador()){

            /*==================================================================================
            =            Se dan valor a las entradas del nuevo producto y se guarda            =
            ==================================================================================*/
            
            $product->nombre = $request->input('Nombre');
            $product->descripcion = $request->input('Descripcion');
            $product->precio = $request->input('Precio');
            $product->UnidadDeMedida = $request->input('UnidadDeMedida');
            $product->marca = $request->input('Marca');

            $product->save();

            /*==================================================================================
            =            Se llama al metodo eventoEmail y se envia mensaje de exito            =
            ==================================================================================*/
            
            $this->eventoEmail();
            
            $message = "Producto agregado correctamente";

            $respuesta = Response::json([

                'System Message' => $message,
                'Data' => $product,
            ], 201);

            return $respuesta;

            /*===========*/
            
        }else{

            return "Acceso no autorizado";

        }
    }

    /**
     * Permite actualizar un producto en base a su ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* Se busca el producto a desear actualizar y se arroja error en caso de no existir */
        
         $product = Product::find($request->id);

         if(!$product){
            return Response::json([

                'error' => [

                    'message' => "No se ha encontrado el producto."

                ]

            ], 404);
         }

         /*===========*/

         $role = Auth::user();

         if($role->Administrador()){

            /*======================================================================================
            =  Se da valor a las entradas a modificar del producto solicitado y se guarda     =
            ======================================================================================*/
            
            $product->nombre = trim($request->nombre);
            $product->descripcion = trim($request->descripcion);
            $product->precio = trim($request->precio);
            $product->UnidadDeMedida = trim($request->UnidadDeMedida);
            $product->marca = trim($request->marca);

            $product->save();

            /*==================================================================================
            =            Se llama al metodo eventoEmail y se envia mensaje de exito            =
            ==================================================================================*/
            
            $this->eventoEmail();

            $message = "El producto ha sido modificado correctamente";

            $respuesta = Response::json([

                'System Message' => $message,
                'Data' => $product,
            ], 201);

            return $respuesta;

            /*===========*/

        }else{

            return "Acceso no autorizado";

        }
    }

    /**
     * Borra un producto en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*===================================================================================
        =                Se busca producto a eliminar mediante su ID                        =
        ===================================================================================*/
        
        $product = Product::find($id);

        if(!$product){
            return Response::json([

                'error' => [

                    'message' => "Producto no existente."

                ]

            ], 404);
        }
        
        /*===========*/

        $role = Auth::user();

        if($role->Administrador()){

            $product->delete();

            /*==================================================================================
            =            Se llama al metodo eventoEmail y se envia mensaje de exito            =
            ==================================================================================*/

            $this->eventoEmail();

            return "Producto eliminado correctamente";

        }else{

            return "Acceso no autorizado";

        }

        
    }

    /**
     * Restaura un producto borrado en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $role = Auth::user();

        if($role->Administrador()){

            /*===================================================================================
            =       Se busca producto a restaurar mediante su ID y lo restauramos       =
            ===================================================================================*/

            Product::withTrashed()->find($id)->restore();

            /*============================================================================
            =            Se invoca metodo email y se muestra mensaje de exito            =
            ============================================================================*/
            
            $this->eventoEmail();

            return "Producto restaurado correctamente";
            

        }else{

            return "Acceso no autorizado";

        }
               
    }
}
