<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Auth
La autenticacion fue hecha mediante api_token el cual fue obtenido mediante tinker con str_random(60)

## Usuarios, Roles y Productos

Los datos internos de roles fueron agregado mediante seeders, igual pude usar endpoints, pero no era parte del proyecto.

En cuanto a los datos del user pude usar seeders o endpoints, sin embargo si usaba seeders no veo que sea correcto que sean estaticos o siempre los mismos, y endpoints no estaba mencionado en el proyecto, por lo que los agregué de manera manual

En cuanto a productos, los endpoints para crear, editar, eliminar y restaurar estan agregados en api.php dentro de routes, con sus especificos auths de administrador Super Admin y Back Office Assistant para ser manipulados

## Evento

Se agregó un metodo de evento de correo en el controlador de producto, el cual funciona con cada metodo a excepcion de index y show 

## Queue

Las queues fueron agregadas mediante el driver database en queue.php, los cuales se guardan en la tabla jobs y se ejecutan con php artisan queue:work

## Mail

Los correos se envian mediante MailTrap con sus especificos User, Password y Mail Encryption

## Lectura de API

La ejecucion de cada ruta API las realizé con PostMan

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
