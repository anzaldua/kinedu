<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([

        	'rol'=>'Super Admin',

        ]);

        DB::table('roles')->insert([

        	'rol'=>'Back Office Assistant',

        ]);

         DB::table('roles')->insert([

        	'rol'=>'Customer',

        ]);
    }
}
