<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

        	'name'=>'Super',
        	'email'=>'superadmin@kinedu.com',
        	'role_id'=>'1',

        ]);

        DB::table('users')->insert([

        	'name'=>'boa',
        	'email'=>'backOffice@kinedu.com',
        	'role_id'=>'2',

        ]);

        DB::table('users')->insert([

        	'name'=>'client',
        	'email'=>'client@kinedu.com',
        	'role_id'=>'3',

        ]);

        DB::table('users')->insert([

        	'name'=>'alex',
        	'email'=>'alex@kinedu.com',
        	'role_id'=>'1',

        ]);

    }
}
